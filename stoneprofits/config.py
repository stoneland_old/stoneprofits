#!/usr/bin/env python

SPS = {
	"url": {
		"list": {
			"accounts": "https://stoneland.stoneprofits.com/listAccounts.aspx",
		    "bills": "https://stoneland.stoneprofits.com/listBills.aspx",
		    "customers": "https://stoneland.stoneprofits.com/listCustomers.aspx",
		    "events": "https://stoneland.stoneprofits.com/listEvents.aspx",
		    "invoices": "https://stoneland.stoneprofits.com/listInvoices.aspx",
		    "items": "https://stoneland.stoneprofits.com/listItems.aspx",
		    "locations": "https://stoneland.stoneprofits.com/listLocations.aspx",
		    "migrations": "https://stoneland.stoneprofits.com/listMigrations.aspx",
		    "openHolds": "https://stoneland.stoneprofits.com/listOpportunities.aspx?tab=5",
		    "salesOrders": "https://stoneland.stoneprofits.com/listSaleOrders.aspx",
		    "purchases": "https://stoneland.stoneprofits.com/listPurchases.aspx",
		    "returns": "https://stoneland.stoneprofits.com/listReturnOrders.aspx",
		    "suppliers": "https://stoneland.stoneprofits.com/listSuppliers.aspx",
		    "users": "https://stoneland.stoneprofits.com/listUsers.aspx",
		    "vendors": "https://stoneland.stoneprofits.com/listVendors.aspx",
		    "instock": "https://stoneland.stoneprofits.com/R_S_InStock.aspx?q=ItemandLocation",
		    "onpo": "https://stoneland.stoneprofits.com/R_S_ONPO.aspx",
		    "intransit": "https://stoneland.stoneprofits.com/listInTransitPurchase.aspx?tab=0",
		    "onhold": "https://stoneland.stoneprofits.com/R_S_OnHold.aspx?q=Item",
		},

	  	"portal": {
    		"admin": "https://stoneland.stoneprofits.com/vAdminTool.aspx",
		    "banking": "https://stoneland.stoneprofits.com/vBankingHome.aspx",
		    "home": "https://stoneland.stoneprofits.com/vHome.aspx",
		    "inventory": "https://stoneland.stoneprofits.com/vInventoryHome.aspx",
		    "ledger": "https://stoneland.stoneprofits.com/vJournalHome.aspx",
		    "lists": "https://stoneland.stoneprofits.com/vMasterList.aspx",
		    "payables": "https://stoneland.stoneprofits.com/vPayable.aspx",
		    "presales": "https://stoneland.stoneprofits.com/vPreSalesHome.aspx",
		    "purchasing": "https://stoneland.stoneprofits.com/vPurchaseHome.aspx",
		    "receivables": "https://stoneland.stoneprofits.com/vReceivable.aspx",
		    "reports": "https://stoneland.stoneprofits.com/vReports.aspx",
		    "sales": "https://stoneland.stoneprofits.com/vSalesHome.aspx",
		    "scheduling": "https://stoneland.stoneprofits.com/vScheduleHome.aspx",
		},

		"utility": {
		    "base": "https://stoneland.stoneprofits.com/",
		    "login": "https://stoneland.stoneprofits.com/default.aspx",
		    "changePassword": "https://stoneland.stoneprofits.com/cChangePassword.aspx",
		    "logout": "https://stoneland.stoneprofits.com/default.aspx?act=logout",
		    "options": "https://stoneland.stoneprofits.com/vSystemOptions.aspx",
		    "quickSearch": "https://stoneland.stoneprofits.com/QuickSearch.aspx",
		},
	
	},

	"admin": {
		"screenshot": "current.png"
	},

	"reports": {
		"all":"reports/all.csv",
		"instock": "reports/instock.csv", 
		"intransit": "reports/intransit.csv",
		"onhold": "reports/onhold.csv",
		"onpo": "reports/onpo.csv",
	}
}

LOGGER = { 
    "version": 1,

    "root": {
    	"handlers": ["console"],
    	"level": "INFO"
    },

    "formatters": { 
        "simple": { 
            "format": "%(asctime)s - %(levelname)s - %(name)s[%(funcName)s] - %(message)s"
        },
    },

    "handlers": {
    	"console": {
    		"level": "INFO",
    		"class": "logging.StreamHandler",
    		"formatter": "simple",
    		"stream": "ext://sys.stdout",
    	}
    },
 
}


    





