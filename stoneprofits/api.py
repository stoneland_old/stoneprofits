#!/usr/bin/env python
# -*- coding: utf-8 -*-

# =======
# Imports
# =======

from __future__ import print_function

from contextlib import contextmanager

import datetime
import logging
import re
import os
import sys
import time

from glob import glob
<<<<<<< HEAD:stoneprofits/api.py

from . import utilities
=======
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/api.py

from . import utilities
from . import config

try:
    import settings
except ImportError:
    pass

<<<<<<< HEAD:stoneprofits/api.py

=======
import pandas as pd
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/api.py
import psycopg2
import requests
import yaml

from bs4 import BeautifulSoup as bs
from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException

<<<<<<< HEAD:stoneprofits/api.py
try:
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
    from selenium.webdriver.support.ui import WebDriverWait, Select
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.common.exceptions import NoSuchElementException, TimeoutException, UnexpectedAlertPresentException
except ImportError:
    print ("the library 'selenium' is not installed, or not on the PATH.")
=======
from sparkpost import SparkPost

import sqlalchemy
from sqlalchemy.dialects import postgresql 
from sqlalchemy.orm import sessionmaker, backref, relationship
from sqlalchemy.ext.automap import automap_base
 
utilities.setup_logging()
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/api.py



# ============
# Main Classes
# ============

class Scraper(object):

    """
    A stoneprofits.com object being able to access all data that is typically accessible on a browser
    """

    # Start a virtual display, open up Chrome and navigate to the SPS homepage
    def __init__(self):
        
        self.logger = logging.getLogger(__name__)
        
        self.config = config.SPS
            
        utilities.lower_keys_in_dict(self.config)
            
        self.logger.info("loaded application configuration")

        self.display = Display(visible=0, size=(2880, 1800)).start()
        self.logger.info("started virtual display")

<<<<<<< HEAD:stoneprofits/api.py
            self.url = self.config['url']['portal']['home']
            self.browser = webdriver.Chrome()
            self.wait = WebDriverWait(self.browser, 10)
            
            self.browser.get(self.url)

            self.logger.info("navigated to {0}".format(self.url))
=======
        self.url = self.config['url']['portal']['home']
        self.browser = webdriver.Chrome()
            
        self.browser.get(self.url)
        self.logger.info("navigated to {0}".format(self.url))
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/api.py

        self.login()           
        
    
    # Find number of pages in current page. Only works for List pages.
    def _find_number_of_pages(self):
        total_pages_id_pattern = re.compile('TotalPages_.*?')
        
        soup = bs(self.browser.page_source, "html.parser")
        
        if soup.find("span", id=total_pages_id_pattern):
            self.logger.info("found total number of pages")
            return int(soup.find("span", id=total_pages_id_pattern).text)
        else:
            return None
            self.logger.info("didn't find total number of pages. check to make sure you're on a list page") 

    
    def _transfer_session_to_requests(self):
        headers = {
        "User-Agent":
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36"
            }
        sesh = requests.session()
        sesh.headers.update(headers)
        for cookie in self.browser.get_cookies():
            c = {cookie['name']: cookie['value']}
            sesh.cookies.update(c)

        return sesh

    
    # Check if logged in.
    def logged_in(self):
        
        logged_in_element = "top_DPloginname"
        base_url = self.config['url']['utility']['base']
        full_name = settings.SPS['fullname']


        if self.browser.current_url.startswith(base_url):
            try:
                if self.browser.find_element_by_id(logged_in_element).text == full_name:
                    self.logger.info("you're logged in")
                    return True
                else:
                    self.logger.warning("not logged in")
                    return False
            except NoSuchElementException as e:
                self.logger.warning("did not find '{}'' on the page. you may not be logged in.".format(full_name))
                return False
        else:
            self.logger.warning("you don't seem to be logged in. call 'current_screen' to see page screenshot")
            return False


    # Login to SPS
    def login(self):
        

        self.url = self.config['url']['utility']['login']
        if len(settings.SPS['username']) > 0:
            username = settings.SPS['username']
        else:
            username = ""
            self.logger.warning("username not set. will not be able to login.")
        
        if len(settings.SPS['password']) > 0:
            password = settings.SPS['password']
        else:
            self.logger.warning("password not set. will not be able to login.")
            password = ""



        if not self.logged_in():

            self.browser.get(self.url)
            self.logger.info("browsed to {0}".format(self.url))
         
            try:
                username_field = self.browser.find_element_by_xpath('//*[@id="cLogin_dbUsername"]')
                username_field.send_keys(username)
            except NoSuchElementException as e:
                self.logger.exception("couldn't find username field: {}".format(e.message))
                
            try:
                pword_field = self.browser.find_element_by_xpath('//*[@id="cLogin_dbPassword"]')
                pword_field.send_keys(password)
            except NoSuchElementException as e:
                self.logger.exception("couldn't find password field: {}".format(e.message))
           
            try: 
                self.browser.find_element_by_xpath('//*[@id="buttonLogin"]').click()
                self.logger.info("logging in")
            except NoSuchElementException as e:
                self.logger.exception("couldn't find button to sign in: {}".format(e.message))

            if self.logged_in():
                self.logger.info("login complete")
                self.browser.current_url
            else:
                self.logger.warning("couldn't complete login")
        else:
            self.logger.info("already logged in")
            pass

            
    # Logout of SPS
    def logout(self):
        self.logout_url = self.config['url']['utility']['logout']
        self.browser.get(self.logout_url)
        self.logger.info("logged out of sps")
        return self.browser.current_url

    # Save current screen in a file    
    def current_screen(self):
        self.browser.save_screenshot(self.config['admin']['screenshot'])
        self.logger.info("saved screenshot of current page to {}".format(self.config['admin']['screenshot']))
        return os.path.abspath(self.config['admin']['screenshot'])

    # Stop the display
    def close(self):
        self.display.stop()
        self.logger.info("virtual display stopped")
 

    # Check if in stock
    def report(self, report_type):
        
        utilities.lower_keys_in_dict(self.config)
        # report_type can be 'instock', 'onhold', 'onpo', and 'intransit'
        # TODO need to add an "all" option
        if report_type == "onpo":
            sd = str(int(datetime.datetime.now().strftime("%Y"))-1) + "-01-01"
            ed = str(int(datetime.datetime.now().strftime("%Y"))) + "-12-31"
            reporturl = self.config['url']['list'][report_type] + "?q1={0}&q2={1}".format(sd, ed)
            self.browser.get(reporturl)
        else:
            try:
                self.browser.get(self.config['url']['list'][report_type])
            except:
                self.logger.error("could not navigate to report page")
        
        soup = bs(self.browser.page_source, "lxml")
        
        # Find the link to Export to Excel, capture in soup
        if soup.find("a", title="Export to Excel"):
            excel_url = self.config['url']['utility']['base'] + soup.find("a", title="Export to Excel").attrs['href']
            s = self._transfer_session_to_requests()
            r = s.get(excel_url)
            s.close()

            #Create a pandas dataframe out of the relevant, nested table.
            newsoup = bs(r.content, "lxml")
            tables = newsoup.find_all("table")

            
            if report_type == "onhold":
                html_table = tables[2]
                data = pd.read_html(str(html_table))[0]
            elif report_type == "intransit":
                html_table = tables[2]
                data = pd.read_html(str(html_table))[0]
            elif report_type == "instock":
                html_table = tables[1]
                data = pd.read_html(str(html_table))[0]
            elif report_type == "onpo":
                html_table = tables[1]
                data = pd.read_html(str(html_table))[0]

            new_header = data.iloc[0]
            data = data[1:]
            data = data.rename(columns = new_header)
            return data
        else:
            self.logger.info("'Export to Excel' not found")
            return None


    # Report is in pandas dataframe format
    def clean_report(self, report):
        
        clean_report = report

        # Uses sensible defaults for dropping and renaming columns
        drop_columns = ["SKU", "Type", "Subcategory", "Units"]
        rename_columns = {"index":"id"}
        
        # Drop columns that are empty
        clean_report.dropna(axis=1, how="all", inplace=True)

        # drop unecessary columns
        for dc in drop_columns:
            try:
                clean_report = clean_report.drop(dc, axis=1)
            except ValueError:
                pass
        
        # drop the last row as it contains the totals
        clean_report.drop(clean_report.index[-1], inplace=True)

        # rename columns
        clean_report.rename(columns=rename_columns, inplace=True)
        
        # lowercase column names, remove spaces and periods
        for c in clean_report.columns:
            clean_report.rename(columns={c:c.lower().strip().replace(" ","_")}, inplace=True)
        # two renames not working in the same for-loop
        for c in clean_report.columns:
            clean_report.rename(columns={c:c.replace(".","")}, inplace=True)

        return clean_report

    def combine_csv(self, combined_csv):
        with open(combined_csv, "w") as f:
            for ef in glob("reports/*.csv"):
                if ef == "reports/intransit.csv" or ef == "reports/onhold.csv" or ef == "reports/onpo.csv":
                    f.write(ef.split("/")[1].split(".")[0])
                    for line in open(ef):
                        f.write(line)
                    f.write("\n\n")

    def combine_csv(self, combined_csv):
        with open(combined_csv, "w") as f:
            for ef in glob(settings.REPORTS['home'] +"*.csv"):
                if ef == settings.REPORTS['intransit'] or ef == settings.REPORTS['onhold'] or ef == settings.REPORTS['onpo']:
                    f.write(ef.split("/")[-1].split(".")[0])
                    for line in open(ef):
                        f.write(line)
                    f.write("\n\n")


class Email(object):

<<<<<<< HEAD:stoneprofits/api.py
    def _holdsurls(self, *args):
        
        self.address = self.config['url']['list']['openholds'] 
        pages = self._find_number_of_pages()
        holdsurl = []
        
        sesh = self._transfer_session_to_requests()

        # get one hold number
        if len(args) == 1:
            hold_no = args[0]
            if pages > 1:
                for x in range(1, pages+1):
                    response = sesh.get(self.config['url']['list']['openholds'] + "&page={0}".format(x))
                    soup = bs(response.content, "lxml")
                    rows = soup.find_all("div", {"class":"LH4_New"})
                    for r in rows:
                        if int(r.text) == hold_no:
                            print ("found", hold_no, "!")
                            butt = r.find("a").attrs["href"].replace("vHold", "cReleaseHold")
                            holdsurl.append(self.config["url"]["utility"]["base"] + butt)
                            break
                    else:
                        continue
                    break
            else:
                response = sesh.get(self.config['url']['list']['openholds'] + "&page=1")
                soup = bs(response.content, "lxml")
                rows = soup.find_all("div", {"class":"LH4_New"})
                for r in rows:
                    if int(r.text) == hold_no:
                        print ("found", hold_no, "!")
                        butt = r.find("a").attrs["href"].replace("vHold", "cReleaseHold")
                        holdsurl.append(self.config["url"]["utility"]["base"] + butt)
                        break
                    
        # get hold numbers in a range
        elif len(args) == 2:
            hold_start_no, hold_end_no = args[0], args[1]
            
            if pages > 1:
                for x in range(1, pages+1):
                    response = sesh.get(self.config['url']['list']['openholds'] + "&page={0}".format(x))
                    soup = bs(response.content, "lxml")
                    rows = soup.find_all("div", {"class":"LH4_New"})
                    for r in rows:
                        if int(r.text) >= hold_start_no and int(r.text) <= hold_end_no:
                            print (r.text)
                            butt = r.find("a").attrs["href"].replace("vHold", "cReleaseHold")
                            holdsurl.append(self.config["url"]["utility"]["base"] + butt)
            else:
                response = sesh.get(self.config['url']['list']['openholds'] + "&page=1")
                soup = bs(response.content, "lxml")
                rows = soup.find_all("div", {"class":"LH4_New"})
                for r in rows:
                    if int(r.text) >= hold_start_no and int(r.text) <= hold_end_no:
                        print (r.text)
                        butt = r.find("a").attrs["href"].replace("vHold", "cReleaseHold")
                        holdsurl.append(self.config["url"]["utility"]["base"] + butt) 
   
        return holdsurl
    
    def cancel_holds(self, holdsurls):
        
        for h in holdsurls:
            try:
                self.address = h
                
                # get hold number from page
                hold_no = None
                tds = self.browser.find_elements_by_xpath("//td[@class='bold']")
                for t in tds:
                    if t.text.lower().startswith("hold #"):
                        hold_no = t.text.split(":")[-1]
                
                # Release Hold
                self.browser.find_element_by_id("btnSubmitHold").click()
                
                # Check if status has changed
                self.wait.until(EC.presence_of_element_located((By.CLASS_NAME, "vT_StatusbarRed")))
                
                statuses = self.browser.find_elements_by_class_name("vT_StatusbarRed")
                
                released = False
                for status in statuses:
                    if status.text.lower().startswith("released"):
                        released = True
                if released:
                    self.logger.info("Hold {0} released".format(hold_no))
                else:
                    self.logger.warning("Hold {0} not released".format(hold_no))

            except UnexpectedAlertPresentException:
                alert = self.browser.switch_to.alert
                alert.accept()
                 
        return None

=======
    def __init__(self):
        
        self.config = settings.SPARKPOST
        utilities.lower_keys_in_dict(self.config)

        self.sparkpost = SparkPost(self.config['password'])
        self.from_email = self.config['from']


    def send_inventory_reports(self, recipients_list, emailtype='report'):
         
        try: 
            response = self.sparkpost.transmissions.send(
                recipients=recipients_list,
                html="<p>See attached inventory reports - 'On Hold', 'In Transit', and 'On PO'</p><br><br><p>--Automated Email--</p>",
                from_email=self.from_email,
                subject="{}: Inventory - On Hold | In Transit | On PO".format(datetime.date.today().strftime("%Y-%m-%d")),
                attachments=[
                    {
                        "name":"inventory.csv",
                        "type":"text/plain",
                        "filename":"{}".format(settings.REPORTS['combined'])
                    }
                ],
                track_opens=True
            )

        except IOError:
            self.logger.warning("reports were not found in the folder")

        return response



"""
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/api.py
class Database(object):

    def __init__(self):

        configfile = utilities.load_app_configuration()

        if os.path.isfile(configfile):
            with open(configfile) as cf:
                config = yaml.load(cf)
            
        utilities.lower_keys_in_dict(config)
        
        self.base = automap_base()

        dbstring = '{}+{}://{}:{}@{}:{}/{}'
        
        self.category = config['db']['type']
        self.user = config['db']['user']
        self.dialect = config['db']['dialect']
        password = config['db']['password']
        self.host = config['db']['host']
        self.port = config['db']['port']
        self.name = config['db']['name']
        

        dbstring = dbstring.format(self.category, self.dialect, self.user, password, self.host, self.port, self.name)
        self.engine = sqlalchemy.create_engine(dbstring, client_encoding='utf-8')

        self.base.prepare(self.engine, reflect=True)


    # Define a session scope context
    @contextmanager
    def session_scope(self):
        # Provide a transactional scope around a series of operations.

        Session = sqlalchemy.orm.sessionmaker(self.engine)
        self.session = Session()
        try:
            yield self.session
            self.session.commit()
        except:
            self.session.rollback()
            raise
        finally:
            self.session.close()

    
    # Returns all data in table as a list of sqlalchemy objects
    def query_all(self, table, *args, **kwargs):
       
        with self.session_scope() as s:
            result = s.query(table).all()
            for r in result:
                s.expunge(r)    # expunge makes result available outside of the session
        
        return result

            

    # Add a primary key to table. Used after porting a pandas dataframe into a table.
    def _add_primary_key(self, tablename):
        with self.session_scope() as s:
            s.execute("ALTER TABLE {} rename column index to id;".format(tablename)) #index added automatically by pandas
            s.execute("ALTER TABLE {} ADD PRIMARY KEY (id);".format(tablename))
            

    # Move pandas dataframe report into database
    def add_report_to_db(self, report, tablename='inventory'):
        
        n = datetime.datetime.now().strftime("%d-%m-%y-t%H-%M-%S")
        report.to_sql(tablename, self.engine, if_exists="replace")
        
        self._add_primary_key(tablename)

        return True
<<<<<<< HEAD:stoneprofits/api.py


class Email(object):

    def __init__(self):
        configfile = utilities.load_app_configuration()
        if os.path.isfile(configfile):
            with open(configfile) as cf:
                self.config = yaml.load(cf)
        
        utilities.lower_keys_in_dict(self.config)

        self.sparkpost = SparkPost(self.config['email']['host_password'])
        self.from_email = self.config['email']['from']


    def send_inventory_reports(self, recipients_list, emailtype='report'):
         
        try: 
            response = self.sparkpost.transmissions.send(
                recipients=recipients_list,
                html="<p>See attached inventory report.</p><br><br><p>--Automated Email--</p>",
                from_email=self.from_email,
                subject="{}: Inventory - On Hold | In Transit | On PO".format(datetime.date.today().strftime("%Y-%m-%d")),
                attachments=[
                    {
                        "name":"not-in-stock-inventory.csv",
                        "type":"text/plain",
                        "filename":"reports/combined.csv"
                    }
                ],
                track_opens=True
            )

        except IOError:
            self.logger.warning("reports were not found in the folder")

        return response
=======
"""
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/api.py

