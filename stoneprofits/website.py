#!/usr/bin/env python
from __future__ import print_function, division

import errno
import logging
import os
import shutil
import subprocess
import sys

from . import api

<<<<<<< HEAD:stoneprofits/website.py
try:
    import numpy
except ImportError:
    print ("'numpy' not installed or not on the PATH")

try:
    import pandas as pd
except ImportError:
    print ("'pandas' is not install or not on the PATH")

try:
    from PIL import Image
except ImportError:
    print ("'pillow' is not installed or not on the PATH")
try:
    from sqlalchemy.ext.automap import automap_base
except ImportError:
    print ("'sqlalchemy' is not installed or not on the PATH")

=======
import numpy
import pandas as pd
from PIL import Image
from sqlalchemy.ext.automap import automap_base
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/website.py

logger = logging.getLogger(__name__)

TOML_TEXT = """
+++
title = "{0}"
modalname = "{1}"
slabs = "{2}"
location = "{3}"
stonetype = "{4}"
category = "{5}"
origin = "{6}"
color_image_location = "{7}"
slab_image_location = "{8}"
+++
"""

GIT_REPO_LOCAL = ""
GIT_REPO_REMOTE = ""


# create folder in the home directory
def __create_folder(repo_name="stonelandinc.com"):
    
    cd = os.path.abspath(os.curdir)

    if sys.argv[0] != "run.py":
        while not os.path.isfile(os.path.join(cd, "run.py")):
            cd = os.path.join(cd, "..")

    try:
        os.makedirs(os.path.join(cd, repo_name))
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    return os.path.join(cd, repo_name)




# git clone repo in the current directory
def clone(remote_repo=GIT_REPO_REMOTE, local_repo=GIT_REPO_LOCAL):
    try:
        subprocess.check_call(['git clone {0} {1}'.format(remote_repo, local_repo)], shell=True)
    except subprocess.CalledProcessError as e:
        print (e.output)
<<<<<<< HEAD:stoneprofits/website.py
    """
    try:
        porcelain.clone(repo_remote, repo_local)
    except GitProtocolError:
        logger.warning("authentication (i.e. ssh keys) isn't setup correctly.")
    except OSError:
        logger.warning("repo already exists. may want to use git pull instead, or rename the repo.")
    """    
=======
  
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/website.py

# git pull to update to the latest website in the current directory
def pull():
    try:
        subprocess.check_call(['git pull'], shell=True)
    except subprocess.CalledProcessError as e:
        print (e.output)
    """
    porcelain.pull(repo_local, repo_remote)
    """
# git add files
def add():
    try:
        subprocess.check_call(['git add --all'], shell=True)
    except subprocess.CalledProcessError as e:
        print (e.output)
    """
    added = porcelain.add(repo)
    return added
    """

# git commit the website. assumes remote is already set up.
def commit(commit_message, repo_local=GIT_REPO_LOCAL):
    try:
        subprocess.check_call(['git commit -m "{}"'.format(commit_message)], shell=True)
    except subprocess.CalledProcessError as e:
        print (e.output)
    """
    if commit_message:
        porcelain.commit(repo_local, commit_message, author="dhruv.kar@stonelandinc.com")
    else:
        logger.error("need a commit message as a second argument to do a 'git commit'")
    """

# git push the website repo
def push():
    try:
        subprocess.check_call(['git push -u origin master'], shell=True)
    except subprocess.CalledProcessError as e:
        print (e.output)
    """
    porcelain.push(repo_local, repo_remote, refs)
    """

# get website directory
def _www_dir(filename="config.toml", dirname="stonelandinc.com"):
    #if sys.argv[0] == "run.py":
    cd = os.path.abspath(os.curdir)
    
    if os.path.isdir(os.path.join(cd, dirname)):
        www = os.path.join(cd, dirname)
        if os.path.isfile(os.path.join(www, filename)):
            return www
        else:
            logger.error("no {} file found. check or try again with a different filename.".format(filename))
    else:
        logger.error("no {} directory found. try again with a different directory name.".format(dirname))


# see if dirty name is functionally the same as a clean name. currently only returns a boolean.
def _match(clean_name, dirty_name):
    confidence = []
    cn = [x.lower() for x in clean_name.split()]
    dn = [x.lower() for x in dirty_name.split()]

    for name in cn:
        if name in dn:
            confidence.append(True)
        else:
            confidence.append(False)
    
    return all(x for x in confidence)      #returns True if all values in confidence list are True



# deletes all files and folders in a directory. ignored files are only ignored in the top-level dir.
def _delete_files_in_folder(folder, ignored_files=[]):
    for f in os.listdir(folder):
        filepath = os.path.join(folder, f)
        try:
            if os.path.isfile(filepath) and f not in ignored_files:
                os.remove(filepath)
            elif os.path.isdir(filepath):
                shutil.rmtree(filepath)
        except Exception as e:
            print (e)


# compare two dataframes and return a dataframe with common items (i.e. in stock)
def compare_df(color_df, raw_df):
    instock = pd.DataFrame()
    
    for i, j in color_df.iterrows():
        for x, y in raw_df.iterrows():
            if _match(j['name'], y['product']):
                instock = instock.append({'name':j['name'],
                                        'type': j['type'],
                                        'category': j['category'],
                                        'origin': j['origin'],
                                        'location': y['location'],
                                        'slabs': y['slabs']}, ignore_index=True)
    
    instock.drop_duplicates(inplace=True)
    #instock.set_index("id", inplace=True)
    return instock
    


# raw_inventory can be a dataframe directly from sps or from a previous stored report
def inventory(raw_clean_inventory):
    if _www_dir():
        if _www_dir("all.csv", "reports"):
            reports_dir = _www_dir("all.csv", "reports")
            all_colors = pd.read_csv(os.path.join(reports_dir, "all.csv"), index_col=0)
            inventory = compare_df(all_colors, raw_clean_inventory)
            return inventory
                

                    
# Generates toml files for catalog based on inventory pandas dataframe
def generate_inventory(inventory_df, toml=TOML_TEXT):
    
    if _www_dir():
        color_path = "/img/colors/"
        catalog_toml_path = os.path.join(_www_dir(), "content", "inventory")
        
        _delete_files_in_folder(catalog_toml_path, ignored_files=["_index.md"])
         
        for c in inventory_df.itertuples():
            no_space_name = c.name.replace(" ", "_")
            no_space_picture = no_space_name + "_color.jpg"
            no_space_slab = no_space_name + "_slab.jpg"
            
            
            if type(c.name) is not float:
                zero = c.name
                one = no_space_name
                seven = os.path.join(color_path, no_space_name, no_space_picture)
                eight = os.path.join(color_path, no_space_name, no_space_slab)
            else:
                zero = ""
                one = ""
                seven = ""
                eight = ""

            two = int(c.slabs)

            if type(c.location) is not float:
                three = c.location
            else:
                three = ""

            if type(c.type) is not float:
                four = c.type
            else:
                four = ""

            if type(c.category) is not float:
                five = c.category
            else:
                five = ""
            
            if type(c.origin) is not float:
                six = c.origin
            else:
                six = ""

  
            with open(os.path.join(catalog_toml_path, "{0}.md".format(no_space_name)), "w") as f:
                f.write(toml.format(zero, one, two, three, four, five, six, seven, eight))
    else:
        logger.error("no hugo website directory found")
        print ("no website dir found")


# Optimize images for web
def optimize_images(start_dir=START_DIR, slab_size=SLAB_SIZE, quality=QUALITY, slab_only=True):
    
    for dp, dn, fn in os.walk(start_dir):
        for f in fn:
            if slab_only:
                if f.endswith(".jpg") and "slab" in f.lower():
                    imagedir = os.path.dirname(os.path.abspath(os.path.join(dp, f)))
                    infile = os.path.join(imagedir, f)
                    outfile = os.path.join(os.path.splitext(infile)[0] + "_optimized" + os.path.splitext(infile)[1])

                    im = Image.open(infile)
                    im = im.resize(slab_size, Image.ANTIALIAS)
                    im.save(outfile, "JPEG", optimize=True, quality=87, progressive=True)

