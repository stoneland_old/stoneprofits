#!/usr/bin/env python
from __future__ import print_function

import os
import subprocess

import stoneprofits

import pandas as pd
import pendulum

"""
kill old processes
"""
stoneprofits.utilities.kill_processes(['chrome', 'xvfb'])


"""
initialize objects
"""
s = stoneprofits.Scraper()
e = stoneprofits.Email()
d = stoneprofits.Database()
n = pendulum.now()
w = stoneprofits.website

"""
get inventory report from sps. capture in a dataframe.
"""
instock = s.clean_report(s.report("instock"))
instock.to_csv("reports/instock.csv")

all_colors = pd.read_csv("reports/all.csv")

inventory_df = w.inventory(instock)

"""
get git repo.
"""
try:
    w.clone()
except:
    pass


"""
generate real-time inventory
"""
if w._www_dir():
    try:
        os.chdir(w._www_dir())
        w.pull()
        w.generate_inventory(inventory_df)
        subprocess.call(['hugo'], shell=True)
        w.add()
        w.commit(commit_message="update inventory for: {}".format(str(n.date())))
        w.push()
    finally:
        os.chdir("..")


"""
generate and send reports
"""
if n.day_of_week == pendulum.FRIDAY: 
    # create pandas dataframe reports from sps
    intransit = s.clean_report(s.report('intransit'))
    onhold = s.clean_report(s.report('onhold'))
    onpo = s.clean_report(s.report('onpo'))

    # save dataframes to csv
    intransit.to_csv("reports/intransit.csv")
    onhold.to_csv("reports/onhold.csv")
    onpo.to_csv("reports/onpo.csv")
    
    s.combine_csv("reports/combined.csv")
    # send csvs to papa, muthu and me
    e.send_inventory_reports(['dhruv.kar@stonelandinc.com', 'lalitkar@stonelandinc.com', 'muthu@stonelandinc.com'])
    #e.send_inventory_reports(['dhruv@kar.ai'])

    # stop the virtual display
    s.close()
