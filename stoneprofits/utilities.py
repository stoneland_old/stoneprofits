#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import errno
import logging
import logging.config

from . import config


import psutil
import yaml


try:
    from PIL import Image
except ImportError:
    print ("'Pillow' is not installed or not on the PATH")

# Lower keys in a dictionary (only to the second level/depth of keys)
def lower_keys_in_dict(c):
    for k1, v1 in c.iteritems():
        if isinstance(v1, dict):
            for k2, v2, in v1.iteritems():
                if isinstance(v2, dict):
                    for k3, v3 in v2.iteritems():
                        v2[k3.lower()] = v2.pop(k3)
                v1[k2.lower()] = v1.pop(k2)
    	c[k1.lower()] = c.pop(k1)


# return the grandparent directory of the file in which this is running
def gp_dir():
    gpdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
    return gpdir

# find config.toml. Assumes script is in a directory below config.toml
def base_dir(config_toml="config.toml"):
    curdir = os.path.dirname(sys.argv[0])
    while not os.path.isfile(os.path.join(curdir, config_toml)):
        curdir = os.path.abspath(os.path.join(curdir, ".."))

    return curdir



# Kill processes with specified name, given in a name
def kill_processes(process_names_list):
    
    for process in psutil.process_iter():
        pinfo = process.as_dict(attrs=['pid', 'name'])

        if len(process_names_list) == 1:
            if pinfo['name'].lower().startswith(process_names_list[0]):
                process.kill()
                print (process)
        else:
            for pn in process_names_list:
                if pinfo['name'].lower().startswith(pn):
                    process.kill()
                    print (process)

<<<<<<< HEAD:stoneprofits/utilities.py
=======
# Call this at the top of other modules
def setup_logging():

    logging.config.dictConfig(config.LOGGER)
    logger = logging.getLogger(__name__)
    logger.info("logger ready to go")

    
>>>>>>> 034f4cb4fbe383014fae0938bcbe657363321d22:stoneprofits/utilities.py
