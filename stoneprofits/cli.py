#!/usr/bin/env python

from __future__ import print_function

import os
import logging

from . import utilities

import click

utilities.setup_logging()
logger = logging.getLogger(__name__)

settings = """
# Need to login to Stoneprofits
SPS = {
  "fullname": "",
  "username": "",
  "password": "",
}

# Need sparkpost to send reports
SPARKPOST = {
  "password": "",
  "from": "",
}

# Full paths to where you want to store reports. home is the 'root' folder for reports. make sure this already exists.
REPORTS = {
    "home": "",
    "all": "",
    "onpo": "",
    "onhold": "",
    "intransit": "",
    "instock": "",
    "combined": "",
    "test": "",
}

# Full paths to website repo. the "_colors" folders will be sub directories of gitlocalrepo
WWW = {
    "gitremoterepo": "",
    "gitlocalrepo": "",
    "raw_colors": "",
    "optimized_colors": "",
}

"""


@click.command()
def new():
    if "settings.py" in os.listdir("."):
        logger.info("'settings.py' file detected. you're good to go.")
        pass
    else:
	      logger.info("created config file. fill in authentication details.")
	      with open("settings.py", "w") as f:
		        f.write(settings)

def main():
	new()

