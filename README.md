# stoneprofits - an (unofficial) SDK for stoneprofits.com

## Getting started

Tested this on Ubuntu Server 16.04.


#### Dependencies

`stoneprofits` runs chrome in a virtual display under the hood to interact with stoneprofits.com. 
You'll need to install *chromedriver* and *chrome*.


***Chromedriver***

To install on Ubuntu Server:

```
$ sudo apt-get install unzip
$ wget http://chromedriver.storage.googleapis.com/2.25/chromedriver_linux64.zip
$ unzip chromedriver_linux64.zip
$ rm chromedriver_linux.zip
$ sudo mv chromedriver /usr/local/bin/
```


***Chrome***

To install chrome on Ubuntu Server:
```
$ wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
$ sudo dpkg -i google-chrome-stable_current_amd64.deb
```

If an error pops up, such as:
```
Errors were encountered while processing:

google-chrome-stable
```

Force install the dependencies:

```
$ sudo apt-get install -f
```


#### Installation

You should now be able to:

`pip install git+https://gitlab.com/stoneland/stoneprofits.git`





