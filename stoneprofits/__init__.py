#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import errno
import logging
import logging.config

try:
    import yaml
except ImportError:
    print ("the library 'pyyaml' is not installed or not on the path")

from .api import Scraper, Database, Email
import utilities

"""
Eventually the directory structure should look like this
.
|--gpdir/
|  |--sps/
|     |--__init__.py
|     |--stoneland.py
|     |--{other modules}
|  |--app_config.yml
|  |--log_config.yml
|  |--.logs/
|  |--Vagrantfile
|  |--playbook.yml
|  |--README.md
"""

####################
# Configure Logging
####################

LOG_CONFIG_FILE = "log_config.yml"
LOG_DIR = ".logs"

        
utilities.create_logs_folder(LOG_DIR)
utilities.load_log_configuration(LOG_DIR, LOG_CONFIG_FILE)
